function showAndScrollTo(id, focus, hideIds = []) {
    $('#'+id).show();
  
    hideIds.forEach((id) => {
      $('#'+id).hide();
    })
  
    if (focus !== null) {
      $('#'+focus).focus();
    }
    $('html, body').animate({scrollTop: $('#'+id).offset().top}, 100);
}